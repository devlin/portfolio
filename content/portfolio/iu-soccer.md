+++
categories = ["game-dev", "unity", "csharp"]
coders = []
date = 2019-08-20T23:00:00Z
description = "Development, Programmer (Unity, VRTK, C#) - VR Soccer Experience Based on IU Sports"
image = "https://i.imgur.com/qKrcSQn.png"
github = []
title = "Indiana University VR Soccer"
type = "post"
[[tech]]
logo = "https://i.imgur.com/v03jzFC.png"
name = "Unity"
url = "https://unity.com/"
[[tech]]
logo = "https://raw.githubusercontent.com/ExtendRealityLtd/related-media/main/github/readme/vrtk.png"
name = "VRTK"
url = "https://www.vrtk.io/"

+++
May 2019 to August 2019

Internship, Small Team

A VR Sports Experience based on Indiana University's Soccer program. This game includes modes where the player plays as the goalie and tries to block as many shots as possible, either from a stationary ball "kicker" or from many moving players that are randomly chosen. Other planned modes include a penalty kick mode using HTC Vive Body trackers to track foot position.

## Features

* Dynamically sized play space for goalies
* Realistic soccer ball physics
* Arcade style goalie rush mode

{{< youtube id="bv4FkgcVCP8" autoplay="false" >}}
