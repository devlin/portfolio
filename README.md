# Portfolio Website

[![License: CC BY 4.0](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)

This is the content of my personal website hosted at [devlinmcclure.com](https://www.devlinmcclure.com). The theme is based on the Hugo Developer Portfolio Theme by Sam Robbins [hugo-developer-portfolio](https://github.com/samrobbins85/hugo-developer-portfolio).
